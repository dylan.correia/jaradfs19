import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import CreateTicket from '../views/CreateTicket.vue'
import TicketDetails from '../views/TicketDetails.vue'
import UpdateTicket from '../views/UpdateTicket.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/tickets/create',
    name: 'CreateTicket',
    component: CreateTicket
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path :'/tickets/:id/update',
    name : 'UpdateTicket',
    component : UpdateTicket
  },
  {
    path :'/tickets/:id',
    name : 'TicketDetails',
    component : TicketDetails
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
