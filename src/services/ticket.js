import axios from '@/utils/axios';

const getTickets = async () => {
  try {
    const { data } = await axios.get('/tickets');
    return data;
  } catch ({ response }) {
    throw response;
  }
};

const getTicket = async (id) => {
  try {
    const { data } = await axios.get(`/tickets/${id}`);
    return data;
  } catch ({ response }) {
    throw response;
  }
};

const postTicket = async (ticket) => {
  try {
    const { data } = await axios.post('/tickets',ticket);
    return data;
  } catch ({ response }) {
    throw response;
  }
};

const updateTicket = async (ticket) => {
  try {
    const { data } = await axios.put(`/tickets/${ticket.id}`,ticket);
    return data;
  } catch ({ response }) {
    throw response;
  }
};

const deleteTicket = async (id) => {
  try {
    const { data } = await axios.delete(`/tickets/${id}`);
    return data;
  } catch ({ response }) {
    throw response;
  }
};

export default {
  getTickets,
  getTicket,
  postTicket,
  deleteTicket,
  updateTicket,
}