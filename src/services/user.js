import axios from '@/utils/axios';

const getUsers = async () => {
  try {
    const { data } = await axios.get('/users');
    return data;
  } catch ({ response }) {
    throw response;
  }
};
const getUser = async (id) => {
  try {
    const { data } = await axios.get(`/users/${id}`);
    return data;
  } catch ({ response }) {
    throw response;
  }
};
const postUser= async (user)=>{
  try {
    const{data} = await axios.post('/user',user);
    return data
  }
  catch ({response}){
    throw response;
  }
}
export default {
  getUsers,
  postUser,
  getUser
}
